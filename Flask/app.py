from flask import Flask
from flask_migrate import Migrate
from database import db

from routes.product_routes import product_bp
from routes.process_routes import process_bp
from routes.energy_source import nrg_bp

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///react-flow-assignment.db'
app.register_blueprint(product_bp, url_prefix='/product')
app.register_blueprint(process_bp, url_prefix='/process')
app.register_blueprint(nrg_bp, url_prefix='/energy')

db.init_app(app)
migrate = Migrate(app, db)

# Import models after initializing db to avoid circular imports
from models.models import Process, Product

if __name__ == '__main__':
    app.run(debug=True)
