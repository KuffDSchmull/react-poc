from flask import Blueprint, request, jsonify
from models.models import NrgSrc, db

nrg_bp = Blueprint('nrg_bp', __name__)

# Add route definitions here
@nrg_bp.route('/add_nrg_src', methods=['POST'])
def add_src():
    data = request.json
    try:
        new_src = NrgSrc( 
            source_name= data['name'],
            co2_emission_per_kwh = data['emissions']
        )
        db.session.add(new_src)
        db.session.commit()
        return jsonify({"message": "Energy Source created successfully", "id": new_src.id}), 201
    except Exception as e:
        db.session.rollback()
        return jsonify({"error": str(e)}), 500
