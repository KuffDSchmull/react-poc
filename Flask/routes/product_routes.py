from flask import Blueprint, request, jsonify
from models.models import Product, db
from sqlalchemy import or_

product_bp = Blueprint('product_bp', __name__)

# Add route definitions here
@product_bp.route('/add_product', methods=['POST'])
def add_product():
    data = request.json
    try:
        p_id = data.get('process_id')
        distance = data.get('transportation_distance')
        new_product = Product(
            name=data['name'],
            size_unit=data['size_unit'],
            weight=data['weight'],
            number_of_cycles=data['number_of_cycles'],
            transportation_distance=distance,
            process_id=p_id
        )
        db.session.add(new_product)
        db.session.commit()
        return jsonify({"message": "Product created successfully", "id": new_product.id}), 201
    except Exception as e:
        db.session.rollback()
        return jsonify({"error": str(e)}), 500
    

@product_bp.route('/get_products/<search>', methods=['GET'])
def get_products(search):
    try:
        # Set an offset (start) and a range (limit)
        start = int(request.args.get('start', 0))
        limit = int(request.args.get('limit', 10))  # Default to 10 products per page

        if search.lower() == 'all':
            query = Product.query
        else:
            # Use ilike for case-insensitive search
            query = Product.query.filter(Product.name.ilike(f'%{search}%'))
        # Apply limit and offset for pagination while querying db
        #products = Product.query.offset(start).limit(limit).all()
        products = query.offset(start).limit(limit).all()
        # make dict from products
        products_list = [{
            'id': product.id,
            'name': product.name,
            'size_unit': product.size_unit,
            'weight': product.weight,
            'number_of_cycles': product.number_of_cycles,
            'transportation_distance': product.transportation_distance,
            'process_id': product.process_id
        } for product in products]

        return jsonify(products_list), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500    

@product_bp.route('/get_product/<int:product_id>', methods=['GET'])
def get_product(product_id):
    try:
        # Query the database for a product with the given ID
        product = Product.query.get(product_id)
        if not product:
            return jsonify({"error": "Product not found"}), 404

        # Create a dictionary from the Product object
        product_data = {
            'id': product.id,
            'name': product.name,
            'size_unit': product.size_unit,
            'weight': product.weight,
            'number_of_cycles': product.number_of_cycles,
            'transportation_distance': product.transportation_distance,
            'process_id': product.process_id
        }

        return jsonify(product_data), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500
