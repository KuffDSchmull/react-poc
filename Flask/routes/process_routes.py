from flask import Blueprint, request, jsonify
from models.models import Process, Product, NrgSrc, db
from queries import create_process
from process_chain import ProcessChain

process_bp = Blueprint('process_bp', __name__)

# Add route definitions here
@process_bp.route('/add_process', methods=['POST'])
def add_process():
    data = request.json
    try:
        wc = data.get('wc')
        nrg = data.get('nrg')
        co2 = data.get('co2')
        prev = data.get('prev') # change this to query the id from db if it exists
        prev_process = Process.query.get(prev) if prev else None
        src = data.get('src') # change this to query the id from db if it exists
        nrg_src = NrgSrc.query.filter_by(source_name=src).first() if src else None
        src_id = nrg_src.id if nrg_src else None
        new_process = Process(
            name = data['name'],
            wc = wc,
            nrg = nrg,
            co2 = co2,
            prev = prev_process.id,
            src = src_id
        )

        prc_id = create_process(new_process.name, new_process.water_consumption, new_process.energy_consumption, new_process.co2_eq, new_process.prev, new_process.nrg_src_id)
        return jsonify({"message": "Process created successfully", "id": prc_id}), 201
    except Exception as e:
        db.session.rollback()
        return jsonify({"error": str(e)}), 500
    
@process_bp.route('/delete_process/<int:process_id>', methods=['DELETE'])
def delete_process(process_id):
    try:
        # Find the process to be deleted
        process_to_delete = Process.query.get(process_id)
        if process_to_delete is None:
            return jsonify({"error": "Process not found"}), 404

        # Find all processes that reference the process to be deleted
        processes_to_update = Process.query.filter_by(next_process_id=process_id).all()
        for process in processes_to_update:
            process.next_process_id = process_to_delete.next_process_id

        # Delete the process
        db.session.delete(process_to_delete)
        db.session.commit()

        return jsonify({"message": "Process deleted successfully"}), 200

    except Exception as e:
        db.session.rollback()
        return jsonify({"error": str(e)}), 500
    
@process_bp.route('/update_process/<int:process_id>', methods=['PUT'])
def update_process(process_id):
    data = request.json
    try:
        # Find the process to be updated
        src = data.get('src') # change this to query the id from db if it exists
        nrg_src = NrgSrc.query.filter_by(source_name=src).first() if src else None
        src_id = nrg_src.id if nrg_src else None
        process = Process.query.get(process_id)
        if process is None:
            return jsonify({"error": "Process not found"}), 404
        
        # Update process details
        process.water_consumption = data.get('wc', process.water_consumption)
        process.energy_consumption = data.get('nrg', process.energy_consumption)
        process.co2_eq = data.get('co2_eq', process.co2_eq)
        process.nrg_src_id = src_id if src_id is not None else process.nrg_src_id

        # Save changes
        db.session.commit()

        return jsonify({"message": "Process updated successfully"}), 200

    except Exception as e:
        db.session.rollback()
        return jsonify({"error": str(e)}), 500
    
@process_bp.route('/get_process_chain/<int:product_id>', methods=['GET'])
def get_process_chain(product_id):
    try:
        # Find the product
        product = Product.query.get(product_id)
        if product is None:
            return jsonify({"error": "Product not found"}), 404

        # Initialize the process chain
        process_chain = ProcessChain(product)

        # Create a list to hold the process chain data
        chain_data = []
        current_process = process_chain.get_head()
        while current_process:
            if current_process.nrg_src and current_process.energy_consumption != 0:
                nrg_src = NrgSrc.query.get(current_process.nrg_src_id)
                co2_factor = nrg_src.co2_emission_per_kwh
                current_process.calc_co2_eq(current_process.energy_consumption, co2_factor)
            process_data = {
                'id': current_process.id,
                'name': current_process.name,
                'water_consumption': current_process.water_consumption,
                'energy_consumption': current_process.energy_consumption,
                'co2_eq': current_process.co2_eq,
                'nrg_src_id': current_process.nrg_src_id,
                'co2_emissions_per_kwh': current_process.co2_eq_nrg
            }
            chain_data.append(process_data)
            current_process = current_process.next_process

        return jsonify(chain_data), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500

