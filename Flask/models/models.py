from database import db

class Process(db.Model):
    __tablename__ = 'process'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    water_consumption = db.Column(db.Float, nullable=True)  # in l/t
    energy_consumption = db.Column(db.Float, nullable=True) # in kWh/t
    co2_eq = db.Column(db.Float, nullable=True) # in gCO2/tkm
    next_process_id = db.Column(db.Integer, db.ForeignKey('process.id'))
    nrg_src_id = db.Column(db.Integer, db.ForeignKey('nrg_src.id'), nullable=True, default=9)
    
    # Relationships
    next_process = db.relationship('Process', remote_side=[id])
    nrg_src = db.relationship('NrgSrc', backref=db.backref('processes', lazy=True))
    #Logic attributes
    prev = None
    co2_eq_nrg = None

    def __init__(self, name, wc,nrg,co2,prev,src):
        self.name = name
        self.water_consumption = wc
        self.energy_consumption = nrg
        self.co2_emission = co2
        self.nrg_src_id = src

        if prev:
            self.prev = prev
       

    # Getters and Setters
    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_water_consumption(self):
        return self.water_consumption

    def set_water_consumption(self, wc):
        self.water_consumption = wc

    def get_energy_consumption(self):
        return self.energy_consumption

    def set_energy_consumption(self, nrg):
        self.energy_consumption = nrg

    def get_co2_eq(self):
        return self.co2_eq

    def set_co2_eq(self, co2):
        self.co2_eq = co2   

    def calc_co2_eq(self,nrg,factor):
        self.co2_eq_nrg = nrg * factor
        return nrg * factor
        

    def set_next_process(self, next_process):
        self.next_process = next_process

    def get_next_process(self):
        return self.next_process


class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    size_unit = db.Column(db.Float, nullable=False) # in liters
    weight = db.Column(db.Integer, nullable=False) # in grams
    number_of_cycles = db.Column(db.Integer, nullable=False)
    transportation_distance = db.Column(db.Float, nullable=True) # in kilometers
    process_id = db.Column(db.Integer, db.ForeignKey('process.id'), nullable = True)

    # Relationship
    process = db.relationship('Process')

    def __init__(self, name, size_unit, weight, number_of_cycles, transportation_distance, process_id):
        self.name = name
        self.size_unit = size_unit
        self.weight = weight
        self.number_of_cycles = number_of_cycles
        self.transportation_distance = transportation_distance
        self.process_id = process_id

    # Getters and Setters
    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

class NrgSrc(db.Model):
    __tablename__ = 'nrg_src'

    id = db.Column(db.Integer, primary_key=True)
    source_name = db.Column(db.String(100), nullable=False)
    co2_emission_per_kwh = db.Column(db.Float, nullable=False) # CO2 emission in gCO2-eq per kWh

    # Relationships

    def __init__(self, source_name, co2_emission_per_kwh):
        self.source_name = source_name
        self.co2_emission_per_kwh = co2_emission_per_kwh

