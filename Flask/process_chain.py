from models.models import Process, Product, NrgSrc , db

class ProcessChain:
    def __init__(self, product):
        self.product = product
        head_process = Process.query.get(self.product.process_id)
        self.head = None
        if head_process:
            self.add_process(head_process)

    def set_head(self, process):
        self.head = process

    def get_head(self):
        return self.head

    def add_process(self, new_process):
        if not self.head:
            self.head = new_process
            return
        last_process = self.head
        while last_process.next_process:
            last_process = last_process.next_process
        last_process.next_process = new_process

    def print_chain(self):
        current_process = self.head
        while current_process:
            print(f'Process: {current_process.get_name()}, Water: {current_process.get_water_consumption()}, Energy: {current_process.get_energy_consumption()}')
            current_process = current_process.get_next_process()
