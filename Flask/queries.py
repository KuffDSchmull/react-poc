from datetime import datetime
import logging
from models.models import Process, db
from flask import jsonify

def create_process(name, wc, nrg, co2, prev_process_id, src=1):
    try:
        logging.info(f"Creating process: name={name}, wc={wc}, nrg={nrg}, co2={co2}, src={src}")
        new_process = Process(name, wc, nrg, co2, prev_process_id, src=src)

        db.session.add(new_process)
        db.session.commit()

        # Linking the new process to the previous process if provided
        if prev_process_id is not None:
            prev_process = Process.query.get(prev_process_id)
            if prev_process:
                prev_process.next_process_id = new_process.id
                db.session.add(prev_process)
            else:
                logging.warning(f"Previous process with ID {prev_process_id} not found.")

        db.session.commit()

        logging.info(f"query ,Process created successfully with ID: {new_process.id}")
        return new_process.id

    except Exception as e:
        logging.error(f"Error in create_process: {e}")
        db.session.rollback()  # Rollback the transaction in case of error
        return str(e)
