/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}", // adjust the path according to your project structure
    // "./public/**/*.html", // include if you use Tailwind classes in your public/index.html
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'white': '#ffffff',
      'purple': '#3f3cbb',
      'midnight': '#121063',
      'metal': '#565584',
      'tahiti': '#3ab7bf',
      'silver': '#ecebff',
      'bubble-gum': '#ff77e9',
      'bermuda': '#78dcca',
      'off-white-blue': '#ECEFF5',
      'off-white-creme':'#F0F0F0',
      'higlight-turquoise':'#18B5AB',
      'title':'#756969',
      'text-default':'#5F5252',
      'sub-title':'#4A4040',
      'placeholder':'#A39C9C',
      'shadow':'#43475C',
      'stroke':'#CCCCCC'

    },
    extend: {},
  },
  plugins: [],
}

