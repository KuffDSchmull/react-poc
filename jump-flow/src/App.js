import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ReactFlowComponent from "./components/ReactFlowComponent";
import SearchPage from './components/SearchPage';
import ProductProcessPage from './components/ProductProcessPage';


function App() {
  return (
    <Router>
      <Routes>
        {//<Route path="/flow" element={<ReactFlowComponent />} />
        }
        <Route path="/" element={<SearchPage />} />
        <Route path="/product/:productId" element={<ProductProcessPage />} />
      </Routes>
    </Router>
  );
}

export default App;
