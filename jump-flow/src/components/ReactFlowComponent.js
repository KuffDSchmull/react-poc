import React, { useEffect, useCallback, useMemo } from 'react';
import Section from './Section'
import ReactFlow, { MiniMap, Controls, Background, useNodesState, useEdgesState, addEdge } from 'reactflow';
import 'reactflow/dist/style.css';

import ProcessNodeComponent from './ProcessNodeComponent';
import Stats from './Stats';

// ... rest of your imports and component code
const handleStyle = { left: 10 };
 
export default function ReactFlowComponent({ product}) {
  console.log(product)
  const containerWidth = window.innerWidth;
  const [nodes, setNodes, onNodesChange] = useNodesState([]);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);
  const nodeTypes = useMemo(() => ({ processNode: ProcessNodeComponent }), []);
  
  const onConnect = useCallback(
    (params) => setEdges((eds) => addEdge(params, eds)),
    [setEdges],
  );
  useEffect(() => {
    fetch(`/process/get_process_chain/${product.id}`)
      .then((response) => response.json())
      .then((data) => {
        const newNodes = data.map((process, index) => ({
          id: process.id.toString(),
          type:'processNode',
          position: { x: containerWidth/2 - 335/2 , y: index *180 +40 }, // Position nodes differently as needed
          data: { label: process.name, wc: process.water_consumption, nrg: process.energy_consumption, co2_km: process.co2_eq, co2_eq: process.co2_emissions_per_kwh }
        }));
        const newEdges = newNodes.slice(1).map((node, i) => ({
          id: `e${newNodes[i].id}-${node.id}`,
          source: newNodes[i].id,
          target: node.id
        }));
        setNodes(newNodes);
        setEdges(newEdges);
      })
      .catch((error) => console.error('Error fetching data:', error));
  }, [product]);
 
  return (
    
    <div style={{ width: '100vw', height: '100vh' }} className='bg-off-white-blue'>
      <Section 
      title="Product"
      CustomComponent={Stats}
      data={product}
      />
      <ReactFlow
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        onConnect={onConnect}
        nodeTypes={nodeTypes}
      
      >
        <Background variant="dots" gap={12} size={1} />
      </ReactFlow>
    </div>
  );
}