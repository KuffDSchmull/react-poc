// components/ProductProcessPage.js
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ReactFlowComponent from './ReactFlowComponent'; // Assuming you have this component ready

function ProductProcessPage() {
  const { productId } = useParams();
  const [product, setProduct] = useState(null);

  useEffect(() => {
    fetch(`/product/get_product/${productId}`)
      .then((response) => response.json())
      .then((data) => setProduct(data));
  }, [productId]);

  return (
    <div>
      {product && (
        <ReactFlowComponent product={product} />
      )}
    </div>
  );
}

export default ProductProcessPage;
