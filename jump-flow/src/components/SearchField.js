// SearchInput.js
import React from 'react';
import './SearchField.css';
import { ReactComponent as MagnifyingGlassIcon } from '../assets/magnifyingGlass.svg'; // Import SVG as React component

function SearchField({ type, placeholder, value, onChange }) {
  return (
    <div className="search-container">
      <div className="search-box">
        <input 
          type={type} 
          placeholder={placeholder} 
          value={value} 
          onChange={onChange}
          className="search-input"
        />
        <div className="search-icon">
          <MagnifyingGlassIcon />
        </div>
      </div>
    </div>
  );
}

export default SearchField;
