import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Section.css';

function Section({ title,data, CustomComponent }) {

  return (
        <div className="section-container">
            <div className="section-card">   
                <div className="section-title">
                    <a className='section-label'>{data.name}</a>
                </div> 
                <CustomComponent data={data}/>
            </div>
        </div>
  );
}

export default Section;
