import { useCallback, React } from 'react';
import { Handle, Position } from 'reactflow';
import LabelItem from './LabelItem'; 
import './ProcessNode.css';
import { ReactComponent as EnergyIcon } from '../assets/dazzle/Energy/bolt.svg';
import { ReactComponent as RouteIcon } from '../assets/dazzle/Maps/route.svg';
import { ReactComponent as CloudIcon } from '../assets/dazzle/Weather/cloud.svg';
import { ReactComponent as WaterIcon } from '../assets/dazzle/Weather/water.svg';

const handleStyle = { left: 10 };
 
function ProcessNodeComponent({ data }) {
  const onChange = useCallback((evt) => {
    console.log(evt.label.value);
  }, []);
 
  return (
    <div className="node-container">
      <Handle type="target" position={Position.Top} />
      <Handle type="source" position={Position.Bottom} />
      <div className="card-background relative bg-off-white-creme rounded-md shadow">

  <div className="titleCard" style={{ width: 234.83, height: 20.93, left: 27, top: 12, position: 'absolute', color: '#4A4040' }}>
  {data.label}
        </div>
  <div className="node-label-items-container">
            <LabelItem
                icon={WaterIcon}
                label={data.wc}
                unit="l/t"
            />
            <LabelItem
                icon={EnergyIcon}
                label={data.nrg}
                unit="kWh/t"
            />
            <LabelItem
                icon={CloudIcon}
                label={data.co2_eq}
                unit="gCO2"
            />
            <LabelItem
                icon={RouteIcon}
                label={data.co2_km}
                unit="g/tkm"
            />
        </div>    
</div>
      
    </div>
  );
}

export default ProcessNodeComponent;
