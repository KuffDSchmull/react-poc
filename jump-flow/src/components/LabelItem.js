// LabelItem.js
import React from 'react';
import './LabelItem.css';
function LabelItem({ icon: Icon, label, unit, className }) {
  return (
    <div className={`label-item ${className || ''}`}>
        <div className='icon-container'>
            {Icon && <Icon className='label-icon'/>}
        </div>  
      <span className="label-text">{label}</span>
      {unit && <span className="label-unit">{unit}</span>}
    </div>
  );
}

export default LabelItem;
