import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Stats.css';
import LabelItem from './LabelItem';
import { ReactComponent as ScaleIcon } from '../assets/dazzle/Money/scale-balanced.svg';
import { ReactComponent as VolumeIcon } from '../assets/dazzle/Weather/droplet.svg';
import { ReactComponent as RecycleIcon } from '../assets/dazzle/Arrow/repeat-alt.svg';
import { ReactComponent as TransportIcon } from '../assets/dazzle/Automotive/truck.svg';
function Stats({ data }) {

  return (
    <div className="stats-label-items-container">
    <LabelItem
        icon={ScaleIcon}
        label={data.weight}
        unit="g"
    />
    <LabelItem
        icon={VolumeIcon}
        label={data.size_unit}
        unit="l"
    />
    <LabelItem
        icon={RecycleIcon}
        label={data.number_of_cycles}
        unit="cycle(s)"
    />
    <LabelItem
        icon={TransportIcon}
        label={data.transportation_distance}
        unit="km"
    />
    </div>   
  );
}

export default Stats;
