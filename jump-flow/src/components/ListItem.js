// ListItem.js
import React from 'react';
import './ListItem.css';
import LabelItem from './LabelItem';
import { ReactComponent as ScaleIcon } from '../assets/dazzle/Money/scale-balanced.svg';
import { ReactComponent as VolumeIcon } from '../assets/dazzle/Weather/droplet.svg';
import { ReactComponent as RecycleIcon } from '../assets/dazzle/Arrow/repeat-alt.svg';
import { ReactComponent as TransportIcon } from '../assets/dazzle/Automotive/truck.svg';

function ListItem({ product, onClick }) {
  return (
    <div className="ListItem" style={{ position: 'relative', cursor: 'pointer' }} onClick={onClick}>
      <div className="NewmorphismCard" style={{   background: '#ECEFF5',position: 'absolute' , borderRadius: 20}}>
        <div className="shadow" style={{   position: 'absolute', boxShadow: '4px 4px 10px 2px rgba(67, 71, 92, 0.25)', borderRadius: 20 }} />
        <div className="highlight" style={{  position: 'absolute', boxShadow: '-4px -4px 8px 2px white', borderRadius: 20 }} />
        <div className="titleCard" style={{ width: 234.83, height: 20.93, left: 27, top: 12, position: 'absolute', color: '#4A4040' }}>
          {product.name}
        </div>
        <div className="label-items-container">
            <LabelItem
                icon={ScaleIcon}
                label={product.weight}
                unit="g"
            />
            <LabelItem
                icon={VolumeIcon}
                label={product.size_unit}
                unit="l"
            />
            <LabelItem
                icon={RecycleIcon}
                label={product.number_of_cycles}
                unit="cycle(s)"
            />
            <LabelItem
                icon={TransportIcon}
                label={product.transportation_distance}
                unit="km"
            />
        </div>    
        
        
      </div>
      
    </div>
  );
}

export default ListItem;

  