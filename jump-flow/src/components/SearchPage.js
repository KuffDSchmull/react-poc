// components/SearchPage.js
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import SearchField from './SearchField';
import ListItem from './ListItem';
import './SearchPage.css';

function SearchPage() {
  const [searchTerm, setSearchTerm] = useState('');
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  const handleSearch = (event) => {
    const value = event.target.value;
    setSearchTerm(value);

    if (value.length > 1) { // Trigger search when the term is longer than 2 characters
      fetch(`/product/get_products/${value}`)
        .then((response) => response.json())
        .then((data) => setProducts(data));
    }
  };

  return (
    <div>
        <div className="search-field-container">
        
        <div className="search-field-card">   
        <div className="section-title">
          <a>Products</a>
        </div> 
        <SearchField
            type="text"
            placeholder="Search Product"
            value={searchTerm}
            onChange={handleSearch}
        />
        </div>
        </div>
        <ul className="product-list">
            {products.map((product) => (
            <ListItem 
                key={product.id} 
                product={product} 
                onClick={() => navigate(`/product/${product.id}`)} 
            />
            ))}
        </ul>
    </div>
  );
}

export default SearchPage;
